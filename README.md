# Steps to create a new project
$ mkdir <project-folder> 
$ cd <project-folder>
$ git clone <deploy-project> deploy
$ git clone <app-repo> app
configure ansible inventory hostnames

# Start local env
$ cd deploy
$ vagrant up local
$ vagrant ssh local
  
run server & tests within VM
make changes in app dir, changes are synced to VM automatically

# Start local_stage env
$ cd deploy
$ vagrant up local\_stage

access on port 80, for example 192.168.50.5
 


# TODO
- staging - add config files from repo instead of local
- seperate tasks, handlers, etc

