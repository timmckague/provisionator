#!/usr/bin/env bash

if [ $(dpkg-query -W -f='${Status}' ansible 2>/dev/null | grep -c "ok installed") -eq 0 ];
then
    echo "Add APT repositories"
    export DEBIAN_FRONTEND=noninteractive
    apt-get install -y software-properties-common
    apt-add-repository -y ppa:ansible/ansible

    apt-get update

    echo "Installing Ansible"
    apt-get install -y ansible
    echo "Ansible installed"
fi
