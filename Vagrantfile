# -*- mode: ruby -*-
# vi: set ft=ruby :

# Vagrantfile API/syntax version. Don't touch unless you know what you're doing!
VAGRANTFILE_API_VERSION = "2"

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|
    # Vbguest plugin configuration.
    config.vbguest.auto_update = true

    # Use Ubuntu 14
    config.vm.box = "ubuntu/trusty64"
    
    config.vm.provider :virtualbox do |vb|
        # This allows symlinks to be created within the /vagrant root directory, 
        # which is something librarian-puppet needs to be able to do. This might
        # be enabled by default depending on what version of VirtualBox is used.
        vb.customize ["setextradata", :id, "VBoxInternal2/SharedFoldersEnableSymlinksCreate/v-root", "1"]

        # Extra config options
        # config.vm.provider "virtualbox" do |vb|
        # Don't boot with headless mode
        # vb.gui = true
        #
        # Use VBoxManage to customize the VM. For example to change memory:
        # vb.customize ["modifyvm", :id, "--memory", "1024"]

    end

    config.ssh.forward_agent = true
    
    config.vm.define :local, primary: true do |local|
        local.vm.network "private_network", ip: "192.168.50.4"
        local.vm.synced_folder "../app",
        "/home/vagrant/sites/local.expensetracker.com/source", :create=> true 
        #local.vm.provision "shell", path: "init.sh" 

        # Ansible provisioner.
        local.vm.provision "ansible" do |ansible|
            ansible.playbook = "local_playbook.yml"
            ansible.inventory_path = "ansible_inventory"
            ansible.host_key_checking = false
            ansible.verbose =  'vvvv'
            ansible.extra_vars = { ansible_ssh_user: 'vagrant', 
                                   ansible_connection: 'ssh',
                                   ansible_limit: 'local',
                                   ansible_ssh_args: '-o ForwardAgent=yes'}
        end

    end

    config.vm.define :local_stage do |local_stage|
        local_stage.vm.network "private_network", ip: "192.168.50.5"
        #local_stage.vm.provision "shell", path: "init.sh" 

        # Ansible provisioner.
        local_stage.vm.provision "ansible" do |ansible|
            ansible.playbook = "playbook.yml"
            ansible.inventory_path = "ansible_inventory"
            ansible.host_key_checking = false
            ansible.verbose =  'vvvv'
            ansible.extra_vars = { ansible_ssh_user: 'vagrant', 
                                   ansible_connection: 'ssh',
                                   ansible_limit: 'local_stage',
                                   ansible_ssh_args: '-o ForwardAgent=yes'}
        end

    end

end
